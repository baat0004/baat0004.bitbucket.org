class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
    setStartingPosition() {
        this._xPos = 1400;
        this._yPos = 177;
    }
}
class CurlStone extends GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
    move(xPosition) {
        this._xPos -= xPosition;
        this._element.classList.add('sliding');
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode == 32) {
                console.log('test');
                this._stone.move(50);
                this.update();
            }
        };
        this._playingField = new PlayingField('playingField');
        this._stone = new CurlStone('stoneBlue_2', 1400, 177);
        this._scoreboard = new Scoreboard('scoreboard');
        this._currentTurn = 0;
        this._goalCircle = new GoalCircle('goalCircle', 0, 130);
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    collision() {
        const goalCircleRect = document.getElementById('goalCircle').getBoundingClientRect();
        const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();
        if (this._currentTurn == 0) {
            const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();
        }
        if (this._currentTurn == 1) {
            const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();
        }
        if (goalCircleRect.right >= curlingStoneRect.left) {
            if (this._currentTurn == 0) {
                this._scoreboard.increaseScore(this._currentTurn);
                this.changePlayer();
            }
            else if (this._currentTurn == 1) {
                this._scoreboard.increaseScore(this._currentTurn);
                this.changePlayer();
            }
        }
    }
    draw() {
        this._stone.draw(this._element);
        this._playingField.draw(this._element);
        this._goalCircle.draw(this._element);
        this._scoreboard.draw(this._element);
    }
    update() {
        this._stone.update();
        this._goalCircle.update();
        this._scoreboard.update();
        this.collision();
    }
    changePlayer() {
        if (this._currentTurn == 0) {
            this._stone.setStartingPosition();
            console.log('Player 2 has to shoot next.');
            this._currentTurn = 1;
        }
        else if (this._currentTurn == 1) {
            this._stone.setStartingPosition();
            console.log('Player 1 has to shoot next.');
            this._currentTurn = 0;
        }
    }
}
class GoalCircle extends GameItem {
    constructor(name, xPosition, yPosition) {
        super(name, xPosition, yPosition);
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class PlayingField extends GameItem {
    constructor(name) {
        super(name);
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score1 = 0;
        this._score2 = 0;
    }
    get score1() {
        return this._score1;
    }
    get score2() {
        return this._score2;
    }
    increaseScore(player) {
        if (player == 0) {
            this._score1 += 1;
        }
        else if (player == 1) {
            this._score2 += 1;
        }
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p1 = document.createElement('p');
        p1.innerHTML = 'The score for player 1 is: ';
        const p2 = document.createElement('p');
        p2.innerHTML = 'The score for player 2 is: ';
        const span1 = document.createElement('span');
        span1.id = 'span1';
        span1.innerHTML = this._score1.toString();
        const span2 = document.createElement('span');
        span2.id = 'span2';
        span2.innerHTML = this._score2.toString();
        p1.appendChild(span1);
        p2.appendChild(span2);
        this._element.appendChild(p1);
        this._element.appendChild(p2);
        container.appendChild(this._element);
    }
    update() {
        var scoreSpan1 = document.getElementById('span1');
        var scoreSpan2 = document.getElementById('span2');
        scoreSpan1.innerHTML = this._score1.toString();
        scoreSpan2.innerHTML = this._score2.toString();
    }
}
//# sourceMappingURL=main.js.map