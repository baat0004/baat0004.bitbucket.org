/// <reference path="gameItem.ts" />

class CurlStone extends GameItem {
    //attributes

    //constructor
    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition)
    }

    //methods

    public move (xPosition: number): void {
        this._xPos -= xPosition;
        this._element.classList.add('sliding')
    }



}