/// <reference path="gameItem.ts" />

class GoalCircle extends GameItem {
    //attributes

    //constructor
    constructor(name: string, xPosition: number, yPosition: number) {
        super(name, xPosition, yPosition);
    }

    //methods
    public draw(container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;

        //create image
        const image = document.createElement('img');
        image.src = `./assets/images/${this._name}.png `;

        //append elements
        this._element.appendChild(image);
        container.appendChild(this._element);
    }

}