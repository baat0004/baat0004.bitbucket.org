/// <reference path="gameItem.ts" />

class Scoreboard extends GameItem {
    //attributes
    private _score1: number;
    private _score2: number;

    //constructor
    constructor(name: string) {
        super(name);
        this._score1 = 0;
        this._score2 = 0;
    }

    //methods
    public get score1(): number {
        return this._score1;
    }
    public get score2(): number {
        return this._score2;
    }

    public increaseScore(player: number): void {
        if (player == 0) {
            this._score1 += 1;
        } else if (player == 1) {
            this._score2 += 1;
        }
    }

    /**
     * Function to draw the initial state of the scoreboard
     * @param {HTMLElement} - container
     */
    public draw (container: HTMLElement): void {
        //create div
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;

        //create p
        const p1 = document.createElement('p');
        p1.innerHTML = 'The score for player 1 is: ';

        const p2 = document.createElement('p');
        p2.innerHTML = 'The score for player 2 is: ';

        //create span
        const span1 = document.createElement('span');
        span1.id = 'span1';
        span1.innerHTML = this._score1.toString();

        const span2 = document.createElement('span');
        span2.id = 'span2';
        span2.innerHTML = this._score2.toString();

        //append elements
        p1.appendChild(span1);
        p2.appendChild(span2);
        this._element.appendChild(p1);
        this._element.appendChild(p2);
        container.appendChild(this._element);
    }

    public update(): void {
        var scoreSpan1 = document.getElementById('span1');
        var scoreSpan2 = document.getElementById('span2');
        scoreSpan1.innerHTML = this._score1.toString();
        scoreSpan2.innerHTML = this._score2.toString();

    }

}