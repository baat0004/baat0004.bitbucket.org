class Game {
    //attributes
    private _element: HTMLElement = document.getElementById('container');
    private _playingField: PlayingField;
    private _stone: CurlStone;
    private _scoreboard: Scoreboard;
    private _currentTurn: number;
    private _goalCircle: GoalCircle;

    /**
     * Create the Game class
     */
    constructor() {
        //create gameItems
        this._playingField = new PlayingField('playingField');
        this._stone = new CurlStone('stoneBlue_2', 1400, 177);
        this._scoreboard = new Scoreboard('scoreboard');
        this._currentTurn = 0;
        this._goalCircle = new GoalCircle('goalCircle', 0, 130);



        //add a keydown handler
        window.addEventListener('keydown', this.keyDownHandler);

        //draw
        this.draw();
    }

    //methods

    public collision(): void {
        //Variables that create a invisible rectangle around the curlingStone and the goal
        const goalCircleRect = document.getElementById('goalCircle').getBoundingClientRect();
        const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();

        //set the ClientRect for the curlingStone
        if (this._currentTurn == 0) {
            const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();
        }
        if (this._currentTurn == 1) {
            const curlingStoneRect = document.getElementById('stoneBlue_2').getBoundingClientRect();
        }

        //set the ClientRect for the goalCircle
        if (goalCircleRect.right >= curlingStoneRect.left) {
            if (this._currentTurn == 0) {
                this._scoreboard.increaseScore(this._currentTurn);
                this.changePlayer();

            }
            else if (this._currentTurn == 1) {
                this._scoreboard.increaseScore(this._currentTurn);
                this.changePlayer();
                

            }
        }
    }

    /**
     * Function to draw the initial state of al living objects
     */
    public draw(): void {
        this._stone.draw(this._element);
        this._playingField.draw(this._element);
        this._goalCircle.draw(this._element);
        this._scoreboard.draw(this._element);
    }

    public update(): void {
        this._stone.update();
        this._goalCircle.update();
        this._scoreboard.update();
        this.collision();
    }


    /**
     * Function to handle the keyboard event
     * @param {KeynboardEvent} - event
     */
    public keyDownHandler = (e: KeyboardEvent): void => {
        if (e.keyCode == 32) {
            console.log('test');

            //move the stone to the left for 50px
            this._stone.move(50);
            this.update();
        }
    }

    private changePlayer(): void {
        if (this._currentTurn == 0) {
            this._stone.setStartingPosition();
            console.log('Player 2 has to shoot next.');
            this._currentTurn = 1;
        } else if (this._currentTurn == 1) {
            this._stone.setStartingPosition();
            console.log('Player 1 has to shoot next.');
            this._currentTurn = 0;
        }
    }
}